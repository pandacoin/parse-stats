'use strict';

// current dependencies can be install using "node i"
const argv = require('minimist')(process.argv.slice(2));
// stats.csv is generated via blockparser's parser all -w 1000000 > stats.csv
const csvFilePath='stats.csv';
const csv=require('csvtojson');
const fs = require('fs').promises;
const cs = require('coinstring');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const targetDir = argv.testnet ? "txs/testnet3" : argv.regtest ? "txs/regtest" : "txs";
const defaultFee = 341980;
const collection = [];
let stats = {};
let total = 0;
let toSend = 0;
let noSend = 0;

String.prototype.withFraction = function () {
	const padded = this.padStart(6, "0");
	return `${padded.slice(0, -6)}.${padded.slice(-6)}`;
}

async function writeTx(cmd, targetFile) {
	const { stdout } = await exec(cmd);
	fs.appendFile(targetFile, stdout);
}

function writeTxs () {
	// TODO reverse this on main net again / add an option for it
	collection.forEach((tx, index) => {
		const { addresses, id, subTotal } = stats[index];
		const { length } = collection.length.toString();
		const padStart = (id + 1).toString().padStart(length, '0');
		const executable = "pandacoin-tx";
		let avPairs = "";
		for (const base58 in tx) {
			const balance = tx[base58];
			avPairs += ` outaddr=${balance.toString().withFraction()}:${base58}`;
		}
		//const targetFile = targetDir + "/" + (500 + (((collection.length - 1) * 7) - index * 7)) + ".json";
		// todo maybe update this?
		const targetFile = `${targetDir}/${index + (!!(argv.testnet || argv.regtest) ? 2 : 6)}.json`;

		let cmd = executable;
		cmd += " -create";
		cmd += " -json";
		cmd += argv.testnet ? " -testnet" : argv.regtest ? " -regtest" : "";
		cmd += avPairs;
		writeTx(cmd, targetFile);
		console.log(`${padStart}/${collection.length.toString()} wrote ${addresses.toString()} target addresses (${subTotal.toString().withFraction()} total) to ${targetFile}`);
	});
}

function main (ledgerRaw) {
	let tx = {};
	let index = 0;
	let txIndex = 0;
	let subTotal = 0;
	let logOnce = 1;
	let subLowerEnd = 0;
	ledgerRaw.forEach(({balance, hash160, base58}) => {
		try {
			balance.trim();
			base58.trim();
			hash160.trim();
		} catch (e) {
			throw `${csvFilePath} contains invalid input. Please re-generate it:\ndocker run -it --rm -v /home/myuser/.pandacoin:/root/.pandacoin:ro blockparser > stats.csv`;
		}

		const hash160Buf = new Buffer.from(hash160, 'hex');
		let version = null;
		if (argv.testnet || argv.regtest) {
			version = (/^P/).test(base58) ? 0x6F : 0xC4;
		} else {
			version = (/^P/).test(base58) ? 0x37 : 0x16;
		}
		base58 = cs.encode(hash160Buf, version);

		// we cannot send below 1 coin, so we pad them
		if (balance.length <= 8) {
			if (logOnce) {
				logOnce--;
				console.log(`Addresses containing less than ${parseFloat("1000000".withFraction())} coins starting at ${base58} (${hash160}). Padding them to 1 coin.`)
			}
			// bail out when we get too many padded balances
			// currently there is 726 addresses with a suitable "low" balance
			subLowerEnd++
			if (subLowerEnd > 3000) throw `Too many padded balances ${subLowerEnd} found. Someone is trying to be funny.`;
			balance = "100000000";
		}
		// peercoin has 6 instead of the regular 8 digit cutoff
		balance = balance.slice(0, -2);

		let int = parseInt(balance);
		let float = parseFloat(balance.withFraction());
		total += int;

		// dev wallet which got lost
		// we can always burn it if the community does collectively disagree about its usage
		if (hash160 === "1c43ec0965ff25e2c3bfd6e36edc15f558fe8601") {
			if (int < 171438225111775) throw `Check balance of ${base58} (${hash160}). Expected ${parseFloat("171438225111775".withFraction())}, got ${float}`;
			console.log(`Adding dev wallet ${base58} (${hash160}) with balance ${float} to no send list.`);
			noSend += int;
			return;
		}
		// another dev wallet which was supposed to be used for marketing and has remained untouched for 6 years
		if (hash160 === "8bd08967a5d63f230c10378907de12fe99cfa81f") {
			if (int < 521395940749238) throw `Check balance of ${base58}. Expected ${parseFloat("521395940749238".withFraction())}, got ${float}`;
			const subtract = 500000000000000;
			console.log(`Adding dev wallet (originally shared with bdanyo) ${base58} (${hash160}) with balance ${subtract.toString().withFraction()} to no send list.`)
			int -= subtract;
			// this doesn't actually need to be updated, but it doesn't hurt either
			float = parseFloat(int.toString().withFraction());
			noSend += subtract;
		}
		// various donation addresses which also got lost
		if (["98a770f9e450dd4051ca92ec5c02617f59d8a804", "4db1ff5b00914a51a073f6e1b93e3b7f888c3ab4", "ccc5fda2e552c5678fc904aaf9e74e02fe3ff07c"].includes(hash160)) {
			console.log(`Adding donor wallet ${base58} (${hash160}) with balance ${float} to no send list.`);
			noSend += int;
			return;
		}
		tx[base58] = int;
		toSend += int;
		subTotal += int;
		const txIndex = collection.length;
		stats[txIndex] = stats[txIndex] ? stats[txIndex] : {};
		stats[txIndex].subTotal = subTotal;
		stats[txIndex].id = txIndex;
		stats[txIndex].addresses = index + 1;
		if ((++index % 1000) === 0) {
			const fee = collection.length ? defaultFee : 341640000;
			collection.push(tx);
			tx = {};
			subTotal = 0;
			index = 0;
			//noSend -= fee;
		}
	});

	if (Object.keys(tx).length) {
		collection.push(tx);
		//noSend -= defaultFee;
	}

	console.log("Total addresses to be padded:", subLowerEnd);
	console.log("Total value to be kept:      ", parseFloat(noSend.toString().withFraction()), "(excluding fee)");
	console.log("Total value to be transacted:", parseFloat(toSend.toString().withFraction()));
	console.log("Total value of network:      ", parseFloat(total.toString().withFraction()));

	if (!targetDir || targetDir.length === 0) throw "Target dir not specified.";
	fs.rmdir(targetDir, { recursive: true })
		.then(() => fs.mkdir(targetDir, { recursive: true }))
		.then(() => writeTxs());
}

csv({delimiter: "\t"})
	.fromFile(csvFilePath)
	.then((jsonObj)=>{
		main(jsonObj);
	});
