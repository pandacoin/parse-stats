#!/bin/bash

cli () {
  # TODO update this for main net
  /home/amdoge/pandacoin/src/pandacoin-cli -rpcuser=rpc -rpcpassword=rpc "$@"
}
HEIGHT="$(cli getblockcount)"
                                     # TODO update this for main net
if [ -f /home/amdoge/parse-stats/txs/"${HEIGHT}".json ]; then
  mkdir -p /home/amdoge/parse-stats/funded
  mkdir -p /home/amdoge/parse-stats/signed
  cli fundrawtransaction "$(jq -sr '.[].hex' /home/amdoge/parse-stats/txs/"${HEIGHT}".json)" "{\"changePosition\":0}" > /home/amdoge/parse-stats/funded/"${HEIGHT}".json
  cli signrawtransaction "$(jq -sr '.[].hex' /home/amdoge/parse-stats/funded/"${HEIGHT}".json)" > /home/amdoge/parse-stats/signed/"${HEIGHT}".json
  cli sendrawtransaction "$(jq -sr '.[].hex' /home/amdoge/parse-stats/signed/"${HEIGHT}".json)" >> /home/amdoge/parse-stats/sent.txt
fi
